var App = {
    launch: function () {
      // alert("starting")
      const age = App.findAge();
      const yrs = App.findYearsWorked();
      const ans = App.findNumYears(age,yrs);
      App.getEstimate();
      // return false;
    },
    // findAge: function () {
    //   let answer = prompt("How old are you?", 35);
    //   if (answer != null) {
    //     //document.getElementById("width").innerHTML = answer;
    //     $('#age').html(answer);   // either double or single tick marks designate strings
    //   }
    // },
    // findYearsWorked: function () {
    //   let answer = prompt("How many years have your worked?", 35);
    //   if (answer != null) {
    //     //document.getElementById("width").innerHTML = answer;
    //     $('#work').html(answer);   // either double or single tick marks designate strings
    //   }
    // },
    findAge: function () {
      let answer = parseFloat($("#EnterAge").val());
      // alert(answer)
      if (answer != null) {
        $('#age').html(answer);
      }
      return answer;
    },
    findYearsWorked: function () {
      let answer = parseFloat($("#EnterWork").val());
      // alert(answer)
      if (answer != null) {
        $('#work').html(answer);
      }
      return answer;
    },
    findNumYears: function (inputAge,inputWorked) {
      // let inputAge = parseFloat($('#age').html());
      // let inputWorked = parseFloat($('#work').html());
      let answer = App.calculateRetire(inputAge, inputWorked);
      $("#totalYears").html(answer);
      $(".displayText").css('display', 'inline-block'); 
      alert("You have worked " + answer + " years.");
    },
    calculateRetire: function (givenAge, givenWorked) {
      if (typeof givenAge !== 'number' || typeof givenWorked !== 'number') {
        throw Error('The given argument is not a number')
        return 0;
      }
      if (isNaN(givenAge) || isNaN(givenWorked)){
        throw Error('The given argument is not a valid')
        return 0;
      }
      const minAge = 1;
      const minWorked = 1;
      const maxAge = 80;
      const maxWorked = 60;
      // check the first argument.................
      let age  // undefined
      if (givenAge < minAge) {
        age = 0;
      }
      else if (givenAge > maxAge) {
        age = 0;
      }
      else {
        age = givenAge;
      }
  
      //check the second argument ...................
      if (givenWorked < minWorked) {
        work = 0;
      }
      else if (givenWorked > maxWorked) {
        work = 0;
      }
      else {
        work = givenWorked;
      }
      // calculate the answer and store in a local variable so we can watch the value
      let totalYears = age + work;
      // return the result of our calculation to the calling function
      return totalYears;
    },
    getEstimate: function () {
      let totalYears = parseFloat(document.getElementById("totalYears").innerHTML);
      let ct;
      if (totalYears < 80) { ct = (80 - totalYears)/2; }
      else { ct = 0 }; // estimate 1 per year
      // document.getElementById("count").innerHTML = count;
      $("#count").html(ct);
      alert("You could retire in " + ct + " year(s).");      
    },
};
  
  